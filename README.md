# ld42

A game for Ludum Dare 42.

Game will be made available [here](http://ectucker.gitlab.io/ld42/) thanks to Gitlab's CI

Check [here](https://ldjam.com/users/ectucker1/games) for results and previous entries.