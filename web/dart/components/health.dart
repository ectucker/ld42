part of game;

class Health extends Component {

  double value;

  bool player;

  double flashTime = 0.0;

  Sound hit;
  Sound death;

  Health(this.value, this.hit, this.death, [this.player = false]);

}

class Damage extends Component {

  double value;

  bool fromPlayer;

  bool bouncing;

  Damage(this.value, [this.fromPlayer = false, this.bouncing = false]);

}