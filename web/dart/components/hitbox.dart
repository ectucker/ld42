part of game;

class Hitbox extends Component {

  double offsetX, offsetY;
  double width, height;

  Obb3 box;

  Hitbox(this.offsetX, this.offsetY, this.width, this.height) {
    box = new Obb3();
    box
      ..center.setFrom(new Vector3(offsetX, offsetY, 0.0))
      ..halfExtents.setFrom(new Vector3(this.width / 2, this.height / 2, 1.0));
  }

  void set angle(double angle) {
    box
      ..resetRotation()
      ..rotate(new Matrix3.rotationZ(radians(angle)));
  }

  void set xy(Vector2 xy) {
    box.center.setValues(xy.x + this.width / 2 + offsetX, xy.y + this.height / 2 + offsetY, 0.0);
  }

}
