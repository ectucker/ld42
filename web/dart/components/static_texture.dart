part of game;

class StaticTexture extends Component {

  Texture _texture;

  String name;

  StaticTexture(this._texture, this.name);

  Texture get texture => _texture;

  void setTexture(Texture texture, String name) {
    this._texture = texture;
    this.name = name;
  }

}
