part of game;

class Scale extends Component {

  double value;

  Scale(this.value);

  Scale.one(): this(1.0);

}

class Flip extends Component {

  bool x;
  bool y;

  Flip(this.x, this.y);

  Flip.none(): this(false, false);

}

class Rotation extends Component {

  double value;

  Rotation(this.value);

  Rotation.none(): this(0.0);

}