part of game;

const int WALL_SIZE = 60;

class EntityBuilder {

  World world;
  var atlas;

  var enemies;

  Entity player;
  Obb3 playerBox;

  CollapseSystem collapse;

  Iterable<Hitbox> solids = [];

  List<Hitbox> hostiles = [];
  List<Entity> hostileEntities = [];

  GameMenu menu;

  Random rand;

  AssetManager assetManager;

  EntityBuilder(this.world, this.atlas, this.enemies, this.menu, this.assetManager) {
    buildWalls();
    player = buildPlayer();
    Hitbox hitbox = player.getComponentByClass(Hitbox);
    playerBox = hitbox.box;
    rand = new Random();
  }

  void buildWalls() {
    Entity bottomWall = world.createEntity();
    bottomWall.addComponent(new Position(0.0, 0.0));
    bottomWall.addComponent(
        new Hitbox(0.0, 0.0, SCREEN_WIDTH.toDouble(), WALL_SIZE.toDouble() / 2));
    bottomWall.addComponent(new Solid());
    bottomWall.addComponent(new Collision());
    bottomWall.addToWorld();

    Entity topWall = world.createEntity();
    topWall.addComponent(new Position(0.0, SCREEN_HEIGHT - WALL_SIZE * 4 + WALL_SIZE / 2));
    topWall.addComponent(
        new Hitbox(0.0, 0.0, SCREEN_WIDTH.toDouble(), WALL_SIZE.toDouble() * 4));
    topWall.addComponent(new Solid());
    topWall.addComponent(new Collision());
    topWall.addToWorld();

    Entity leftWall = world.createEntity();
    leftWall.addComponent(new Position(0.0, 0.0));
    leftWall.addComponent(new Hitbox(0.0, 0.0, WALL_SIZE.toDouble() / 2,
        SCREEN_HEIGHT.toDouble()));
    leftWall.addComponent(new Solid());
    leftWall.addComponent(new Collision());
    leftWall.addToWorld();

    Entity rightWall = world.createEntity();
    rightWall.addComponent(new Position(SCREEN_WIDTH - WALL_SIZE / 2, 0.0));
    rightWall.addComponent(new Hitbox(0.0, 0.0, WALL_SIZE.toDouble() / 2,
        SCREEN_HEIGHT.toDouble()));
    rightWall.addComponent(new Solid());
    rightWall.addComponent(new Collision());
    rightWall.addToWorld();
  }

  Entity buildPlayer() {
    Entity player = world.createEntity();
    player.addComponent(new Position(1920 / 2, 1080 / 2));
    player.addComponent(new Hitbox(6.0, 6.0, 51.0, 105.0));
    player.addComponent(new PlayerControl());
    player.addComponent(new StaticTexture(atlas['player1_b'], 'player1_b'));
    player.addComponent(new Acceleration(0.0, 0.0));
    player.addComponent(new Velocity(0.0, 0.0));
    player.addComponent(new CollisionList());
    player.addComponent(new Health(100.0, null, null, true));
    player.addComponent(new MaxSpeed(200.0));
    player.addComponent(new Flip.none());
    //Entity gun = buildGun();
    //player.addComponent(new Weapon(gun));
    player.addToWorld();
    //gun.addToWorld();
    return player;
  }

  Entity buildGun() {
    Entity gun = world.createEntity();
    gun.addComponent(new Position(0.0, 0.0));
    gun.addComponent(new Rotation(0.0));
    gun.addComponent(new Flip.none());
    gun.addComponent(new StaticTexture(atlas['staff_s'], 'staff_s'));
    return gun;
  }

  void buildCollapse(var map, Vector2 position) {
    position /= 60.0;
    position.x = position.x.roundToDouble();
    position.y = position.y.roundToDouble();

    int tileX = position.x.toInt() - 1;
    int tileY = position.y.toInt() - 1;
    if(tileY < 0) {
      tileY = 0;
    }
    if(tileX < 0) {
      tileX = 0;
    }

    if(map[tileX][tileY] < 1) {
      Hitbox hitbox = new Hitbox(0.0, 0.0, 60.0, 60.0);
      hitbox.xy = new Vector2(tileX * 60.0 + 30.0, tileY * 60.0 + 30.0);
      if (!hitbox.box.intersectsWithObb3(playerBox) && !(map[tileX][tileY] > 0)) {
        buildCollapseTile(tileX, tileY);
        map[tileX][tileY] = 1;

        for(int i = 0; i < map.length; i++) {
          for(int j = 0; j < map[0].length; j++) {
            if(map[i][j] < 0) {
              map[i][j] = 0;
            }
          }
        }

        Position playerPos = player.getComponentByClass(Position);

        int playerTileX = (playerPos.x / 60.0 - 0.5).round();
        int playerTileY = (playerPos.y / 60.0).round();

        flood(map, playerTileX, playerTileY, 0, -1);

        for(int i = 0; i < map.length; i++) {
          for(int j = 0; j < map[0].length; j++) {
            if(map[i][j] == 0) {
              map[i][j] = 1;
              buildCollapseTile(i, j);
            }
          }
        }
      }
    }
  }

  void flood(map, int x, int y, int target, int replacement) {
    if(x < 0 || y < 0 || x >= map.length || y >= map[0].length) {
      return;
    }
    if(map[x][y] != target) {
      return;
    }
    map[x][y] = replacement;
    flood(map, x + 1, y, target, replacement);
    flood(map, x - 1, y, target, replacement);
    flood(map, x, y + 1, target, replacement);
    flood(map, x, y - 1, target, replacement);
  }

  void buildCollapseTile(int tileX, int tileY) {
    Hitbox hitbox = new Hitbox(0.0, 0.0, 60.0, 60.0);
    hitbox.xy = new Vector2(tileX * 60.0 + 30.0, tileY * 60.0 + 30.0);

    Entity collapse = world.createEntity();
    collapse.addComponent(new Position(tileX * 60.0 + 30.0, tileY * 60.0 + 30.0));
    collapse.addComponent(hitbox);
    collapse.addComponent(new Collision());
    collapse.addComponent(new Solid());
    int tileNum = rand.nextInt(6) + 1;
    collapse.addComponent(new StaticTexture(atlas["block${tileNum}"], "block${tileNum}"));
    collapse.addToWorld();

    for(int i = 0; i < hostiles.length; i++) {
      if(hostiles[i].box.intersectsWithObb3(hitbox.box)) {
        hostileEntities[i].deleteFromWorld();
      }
    }
  }

  /* PROJECTILES */

  Entity buildPlayerBullet(Vector2 source, Vector2 target) {
    Entity bullet = world.createEntity();
    bullet.addComponent(new Position.fromVec(source));
    bullet.addComponent(new Hitbox(24.0, 54.0, 40.0, 44.0));
    bullet.addComponent(
        new Velocity.fromVec((target - source).normalized() * 350.0));
    bullet.addComponent(new CollisionList());
    bullet.addComponent(new CollisionRemove());
    bullet.addComponent(new Collapse());
    bullet.addComponent(new Damage(10.0, true));
    bullet.addComponent(new StaticTexture(atlas['fireball'], 'fireball'));
    bullet.addComponent(new Flip.none());
    bullet.addComponent(new FlipRepeat());
    bullet.addComponent(new Rotation(degrees(atan2(target.y - source.y, target.x - source.x) + PI) - 90));
    bullet.addToWorld();
    return bullet;
  }

  Entity buildBasicBullet(Vector2 source, Vector2 target, double speed, double damage, Texture texture, String textureName,
      double width, double height, double offsetX, double offsetY) {
    Entity bullet = world.createEntity();
    bullet.addComponent(new Position.fromVec(source.clone()));
    bullet.addComponent(new Hitbox(offsetX, offsetY, width, height));
    bullet.addComponent(
        new Velocity.fromVec((target - source).normalized() * speed));
    bullet.addComponent(new CollisionList());
    bullet.addComponent(new CollisionRemove());
    bullet.addComponent(new Damage(damage));
    bullet.addComponent(new StaticTexture(texture, textureName));
    bullet.addComponent(new Rotation(degrees(atan2(target.y - source.y, target.x - source.x) + PI)));
    bullet.addToWorld();
    return bullet;
  }

  void buildChandelierBullets(Vector2 source, double startAngle) {
    assetManager.get('candlestickShot.ogg').play();
    for(int i = 1; i <= 8; i++) {
      buildBasicBullet(source, source + new Vector2(cos(radians(startAngle + 360 / 8 * i)), sin(radians(startAngle + 360 / 8 * i))),
          enemies['chandelier']['bulletSpeed'], enemies['chandelier']['bulletDamage'],
          atlas['fire'], 'fire', 21.0, 21.0, 11.0, 9.0);
    }
  }

  buildTeapotBullets(Vector2 source, Vector2 target, bool flip) {
    assetManager.get('teapotShot.ogg').play();
    double midAngle = atan2((target - source).x, (target - source).y);
    double theta = 18.75 / 180 * PI;
    for(double i = -1.5; i <= 1.5; i++) {
      double launchAngle = midAngle + i * theta;
      Vector2 newDirection = new Vector2(sin(launchAngle), cos(launchAngle));
      var bullet = buildBasicBullet(source, source + newDirection, enemies['teapot']['bulletSpeed'],
          enemies['teapot']['bulletDamage'], atlas['tea'], 'tea', 52.0, 32.0, 11.0, 12.0);
      bullet.addComponent(new Flip(false, flip));
    }
  }

  buildKnifeBullet(Vector2 source, Vector2 target, bool flip) {
    assetManager.get('knifeShot.ogg').play();
    Entity bullet = buildBasicBullet(source, target, enemies['knives']['bulletSpeed'],
        enemies['knives']['bulletDamage'],
        atlas['knifebullet'], 'knifebullet', 80.0, 14.0, 14.0, 21.0);
    bullet.addComponent(new Flip(false, flip));
  }

  buildSkeleBullet(Vector2 source, Vector2 target, Entity owner) {
    assetManager.get('skeletonShot.ogg').play();
    Entity bullet = world.createEntity();
    bullet.addComponent(new Position.fromVec(source.clone()));
    bullet.addComponent(new Hitbox(9.0, 11.0, 50.0, 27.0));
    bullet.addComponent(
        new Velocity.fromVec((target - source).normalized() * enemies['skelly']['bulletSpeed']));
    bullet.addComponent(new CollisionList());
    bullet.addComponent(new Damage(enemies['skelly']['bulletDamage'], false, true));
    bullet.addComponent(new StaticTexture(atlas['arm'], 'bullet'));
    var returnBone = new Return(enemies['skelly']['fireRate'], owner);
    bullet.addComponent(returnBone);
    bullet.addComponent(new Rotation.none());
    bullet.addToWorld();
    return returnBone;
  }

  void buildDusterBullets(Vector2 source, Vector2 target) {
    assetManager.get('dusterShot.ogg').play();
    Vector2 direction = target - source;
    Vector2 perpendicular = new Vector2(-direction.y, direction.x).normalized();
    for(int i = 0; i <= 2; i++) {
      Entity shot = buildBasicBullet(source + perpendicular * i.toDouble() * 32.0, target,
          enemies['duster']['bulletSpeed'], enemies['duster']['bulletDamage'],
          atlas['feather'], 'feather', 75.0, 20.0, 32.0, 29.0);
      Velocity vel = shot.getComponentByClass(Velocity);
      vel.xy = (target - source).normalized() * enemies['duster']['bulletSpeed'];
      Rotation rot = shot.getComponentByClass(Rotation);
      rot.value = degrees(atan2(target.y - source.y, target.x - source.x) + PI);
    }
  }

  /* ENEMY UTILITIES */

  bool overlapsSolid(Hitbox hitbox) {
    for(Hitbox solid in solids) {
      if(solid.box.intersectsWithObb3(hitbox.box)) {
        return true;
      }
    }
    return false;
  }

  List<Vector2> randEmptyTiles() {
    List<Vector2> emptyTiles = [];
    for(int i = 0; i < collapse.map.length; i++) {
      for(int j = 0; j < collapse.map[i].length; j++) {
        if(collapse.map[i][j] <= 0) {
          emptyTiles.add(new Vector2(i.toDouble(), j.toDouble()));
        }
      }
    }
    emptyTiles.shuffle();
    return emptyTiles;
  }

  Vector2 getValidSpawn(Hitbox hitbox) {
    var tiles = randEmptyTiles();
    for(Vector2 tile in tiles) {
      hitbox.xy = tile * 60.0 + new Vector2(31.0, 31.0);
      if(!overlapsSolid(hitbox) && !(hitbox.box.center.distanceTo(playerBox.center) < 200)) {
        return tile * 60.0 + new Vector2(40.0, 40.0);
      }
    }
    return getOverlappingSpawn(hitbox);
  }

  Vector2 getOverlappingSpawn(Hitbox hitbox) {
    var tiles = randEmptyTiles();
    for(Vector2 tile in tiles) {
      hitbox.xy = tile * 60.0 + new Vector2(31.0, 31.0);
      if(!overlapsSolid(hitbox)) {
        return tile * 60.0 + new Vector2(31.0, 31.0);
      }
    }
    return null;
  }

  /* ENEMIES */

  Entity buildBasicEnemy(double health, double speed, double width, double height, double offsetX, double offsetY, Sound hitsound, Sound deathsound) {
    if(randEmptyTiles().length < 12) {
      menu.showDeath();
      PlayerControlSystem control = world.getSystem(PlayerControlSystem);
      control.stop = true;
    }

    Entity enemy = world.createEntity();

    Hitbox hitbox = new Hitbox(offsetX, offsetY, width, height);
    Vector2 spawn = getValidSpawn(hitbox) - new Vector2(hitbox.offsetX, hitbox.offsetY);
    if(spawn == null) {
      return null;
    }

    enemy.addComponent(hitbox);
    enemy.addComponent(new Position.fromVec(spawn));
    enemy.addComponent(new Velocity(0.0, 0.0));
    enemy.addComponent(new CollisionList());
    enemy.addComponent(new Health(health, hitsound, deathsound));
    enemy.addComponent(new MaxSpeed(speed));
    enemy.addComponent(new Hostile());
    enemy.addComponent(new Acceleration(0.0, 0.0));
    enemy.addComponent(new Rotation(0.0));
    enemy.addComponent(new Flip.none());

    return enemy;
  }
  
  Entity buildChandelier() {
    var chandelierData = enemies['chandelier'];
    Entity chandelier = buildBasicEnemy(chandelierData['health'], chandelierData['speed'],
        75.0, 148.0, 111.0, 71.0,
        assetManager.get('candlestickHit.ogg'), assetManager.get('candlestickDeath.ogg'));
    if(chandelier == null) {
      return null;
    }

    chandelier.addComponent(new StaticTexture(atlas['candle'], 'candle'));
    chandelier.addComponent(new ChandelierAI());
    chandelier.addToWorld();

    assetManager.get('candlestickBattlecry.ogg').play();

    return chandelier;
  }

  Entity buildGhost() {
    var ghostData = enemies['ghost'];
    Entity ghost = buildBasicEnemy(ghostData['health'], ghostData['speed'],
        54.0, 85.0, 37.0, 35.0,
        assetManager.get('ghostDeath.ogg'), assetManager.get('ghostDeath.ogg'));
    if(ghost == null) {
      return null;
    }

    ghost.addComponent(new StaticTexture(atlas['ghost1'], 'ghost1'));
    ghost.addComponent(new GhostAI());
    ghost.addComponent(new Damage(ghostData['bulletDamage']));
    ghost.addToWorld();

    assetManager.get('ghostBattlecry.ogg').play();

    return ghost;
  }

  Entity buildTeapot() {
    var teapotData = enemies['teapot'];
    Entity teapot = buildBasicEnemy(teapotData['health'], teapotData['speed'],
        79.0, 68.0, 42.0, 34.0,
        assetManager.get('teapotHit.ogg'), assetManager.get('teapotDeath.ogg'));
    if(teapot == null) {
      return null;
    }

    teapot.addComponent(new StaticTexture(atlas['teapot1'], 'teapot1'));
    teapot.addComponent(new TeapotAI());
    teapot.addToWorld();

    assetManager.get('teapotBattlecry.ogg').play();

    return teapot;
  }

  Entity buildKnife() {
    var knifeData = enemies['knives'];
    Entity knife = buildBasicEnemy(knifeData['health'], knifeData['speed'],
        190.0, 101.0, 25.0, 41.0,
        assetManager.get('knifeHit.ogg'), assetManager.get('knifeDeath.ogg'));
    if(knife == null) {
      return null;
    }

    knife.addComponent(new StaticTexture(atlas['knife1'], 'knife1'));
    knife.addComponent(new KnifeAI());
    knife.addToWorld();

    assetManager.get('knifeBattlecry.ogg').play();

    return knife;
  }

  Entity buildSkele() {
    var skeleData = enemies['skelly'];
    Entity skele = buildBasicEnemy(skeleData['health'], skeleData['speed'],
        42.0, 158.0, 30.0, 9.0,
        assetManager.get('skeletonHit.ogg'), assetManager.get('skeletonDeath.ogg'));
    if(skele == null) {
      return null;
    }

    skele.addComponent(new StaticTexture(atlas['skeleton1'], 'skeleton1'));
    skele.addComponent(new SkeleAI());
    skele.addToWorld();

    assetManager.get('skeletonBattlecry.ogg').play();

    return skele;
  }

  Entity buildDuster() {
    var dusterData = enemies['duster'];
    Entity duster = buildBasicEnemy(dusterData['health'], dusterData['speed'],
        225.0, 60.0, 62.0, 57.0,
        assetManager.get('dusterHit.ogg'), assetManager.get('dusterDeath.ogg'));
    if(duster == null) {
      return null;
    }

    duster.addComponent(new StaticTexture(atlas['duster1'], 'duster1'));
    duster.addComponent(new DusterAI());
    duster.addToWorld();

    assetManager.get('dusterBattlecry.ogg').play();

    return duster;
  }

}