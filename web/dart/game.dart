part of game;

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

var gAssetManager;

class Game extends BaseGame {

  State state, push;

  var atlas;

  pushState(State state) {
    this.state?.pause();
    push = state;
    push.preload();
  }

  @override
  create() {
    atlas = assetManager.get("atlas");

    gl.setGLViewport(canvasWidth, canvasHeight);
    pushState(new MainMenuState(this));
    resize(width, height);
  }

  @override
  preload() {
    gAssetManager = assetManager;

    assetManager.load("atlas", loadAtlas("img/atlas.json", loadTexture(gl, "img/atlas.png", nearest)));
    assetManager.load("lighting", loadProgram(gl, "glsl/basic.vert", "glsl/lighting.frag"));
    assetManager.load("enemies.yaml", loadYaml("data/enemies.yaml"));
    assetManager.load("waves.yaml", loadYaml("data/waves.yaml"));
    assetManager.load('ingame.html', HttpRequest.getString('html/ingame.html'));
    assetManager.load('gunshot.ogg', loadSound(audio, 'snd/gunshot4.ogg'));
    assetManager.load('roll.ogg', loadSound(audio, 'snd/rolling.ogg'));
    assetManager.load('collapse.ogg', loadSound(audio, 'snd/collapse.ogg'));
    assetManager.load('hurt.ogg', loadSound(audio, 'snd/hitsound.ogg'));
    assetManager.load('title.ogg', loadSound(audio, 'snd/title.ogg'));
    assetManager.load('spooky.ogg', loadSound(audio, 'snd/spooky.ogg'));
    assetManager.load('countdown.ogg', loadSound(audio, 'snd/countdown.ogg'));
    assetManager.load('start.ogg', loadSound(audio, 'snd/start.ogg'));
    
    assetManager.load('candlestickHit.ogg', loadSound(audio, 'snd/candlestickHit.ogg'));
    assetManager.load('candlestickBattlecry.ogg', loadSound(audio, 'snd/candlestickBattlecry.ogg'));
    assetManager.load('candlestickDeath.ogg', loadSound(audio, 'snd/candlestickDeath.ogg'));
    assetManager.load('candlestickShot.ogg', loadSound(audio, 'snd/candlestickShot.ogg'));

    assetManager.load('dusterHit.ogg', loadSound(audio, 'snd/dusterHit.ogg'));
    assetManager.load('dusterBattlecry.ogg', loadSound(audio, 'snd/dusterBattlecry.ogg'));
    assetManager.load('dusterDeath.ogg', loadSound(audio, 'snd/dusterDeath.ogg'));
    assetManager.load('dusterShot.ogg', loadSound(audio, 'snd/dusterShot.ogg'));

    assetManager.load('ghostBattlecry.ogg', loadSound(audio, 'snd/ghostBattlecry.ogg'));
    assetManager.load('ghostDeath.ogg', loadSound(audio, 'snd/ghostDeath.ogg'));

    assetManager.load('knifeHit.ogg', loadSound(audio, 'snd/knifeHit.ogg'));
    assetManager.load('knifeBattlecry.ogg', loadSound(audio, 'snd/knifeBattlecry.ogg'));
    assetManager.load('knifeDeath.ogg', loadSound(audio, 'snd/knifeDeath.ogg'));
    assetManager.load('knifeShot.ogg', loadSound(audio, 'snd/knifeShot.ogg'));

    assetManager.load('skeletonHit.ogg', loadSound(audio, 'snd/skeletonHit.ogg'));
    assetManager.load('skeletonBattlecry.ogg', loadSound(audio, 'snd/skeletonBattlecry.ogg'));
    assetManager.load('skeletonDeath.ogg', loadSound(audio, 'snd/skeletonDeath.ogg'));
    assetManager.load('skeletonShot.ogg', loadSound(audio, 'snd/skeletonShot.ogg'));

    assetManager.load('teapotHit.ogg', loadSound(audio, 'snd/teapotHit.ogg'));
    assetManager.load('teapotBattlecry.ogg', loadSound(audio, 'snd/teapotBattlecry.ogg'));
    assetManager.load('teapotDeath.ogg', loadSound(audio, 'snd/teapotDeath.ogg'));
    assetManager.load('teapotShot.ogg', loadSound(audio, 'snd/teapotShot.ogg'));
  }

  @override
  update(num delta) {
    if (assetManager.allLoaded()) {
      push?.create();
      push?.resume();
      state = push ?? state;
      push = null;
    }

    state?.update(delta);
  }

  @override
  render(num delta) {
    gl.clearScreen(Colors.black);

    state?.render(delta);
  }

  @override
  pause() {
    state?.pause();
  }

  @override
  resume() {
    state?.resume();
  }

  @override
  resize(width, height) {
    gl.setGLViewport(canvasWidth, canvasHeight);

    state?.resize(width, height);

    DivElement ui = querySelector('#ui');
    DivElement ui2 = querySelector('#ui2');

    num scale = min(window.innerWidth / SCREEN_WIDTH, window.innerHeight / SCREEN_HEIGHT);

    ui.style.width = (SCREEN_WIDTH * scale).toString() + "px";
    ui.style.height = (SCREEN_HEIGHT * scale).toString() + "px";
    ui2.style.width = (SCREEN_WIDTH * scale).toString() + "px";
    ui2.style.height = (SCREEN_HEIGHT * scale).toString() + "px";
  }

  @override
  config() {
    scaleMode = ScaleMode.fit;
    requestedWidth = SCREEN_WIDTH;
    requestedHeight = SCREEN_HEIGHT;
  }

}
