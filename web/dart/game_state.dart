part of game;

class GameState extends MenuState {

  Game game;
  World world;

  var atlas;

  RenderSystem renderer;

  bool madness = true;

  Music background;

  GameState(this.game): super(new GameMenu(game)) {
    atlas = game.atlas;
    world = new World();
  }

  @override
  create() {
    super.create();

    EntityBuilder builder = new EntityBuilder(world, atlas, assetManager.get("enemies.yaml"), menu, assetManager);
    var player = builder.player;

    var enemies = assetManager.get("enemies.yaml");

    renderer = new RenderSystem(gl, width, height, canvas, assetManager);
    world.addSystem(new PlayerControlSystem(keyboard, mouse, builder, renderer, tweenManager, atlas));
    var waves = new WaveSystem(assetManager.get("waves.yaml"), builder, menu, player, this);
    world.addSystem(waves);
    world.addSystem(new ChandelierAISystem(builder, player, enemies, atlas));
    world.addSystem(new GhostAISystem(builder, player, enemies, atlas));
    world.addSystem(new TeapotAISystem(builder, player, enemies, atlas));
    world.addSystem(new DusterAISystem(builder, player, enemies, atlas));
    world.addSystem(new KnifeAISystem(builder, player, enemies, atlas));
    world.addSystem(new SkeleAISystem(builder, player, enemies, atlas));
    world.addSystem(new AccelerationSystem());
    world.addSystem(new MovementSystem());
    world.addSystem(new HitboxTransformSystem());
    world.addSystem(new SolidList(builder));
    world.addSystem(new HostileList(builder));
    world.addSystem(new DamageList(waves));
    world.addSystem(new CollisionListSystem());
    world.addSystem(new DamageSystem(menu, this));
    world.addSystem(new ReturnSystem());
    var collapse = new CollapseSystem(builder, player);
    world.addSystem(collapse);
    builder.collapse = collapse;
    world.addSystem(new CollisionRemoveSystem());
    world.addSystem(new AntiMovementSystem());
    world.addSystem(new FlipRepeatSystem());
    world.addSystem(new DamageFlashSystem(atlas));
    world.addSystem(renderer);
    world.initialize();

    background = assetManager.get("spooky.ogg");
    background.loop();
  }

  @override
  update(num delta) {}

  @override
  render(num delta) {
    if(madness) {
      world.delta = delta;
    } else {
      world.delta = 0.0;
      if(world.getSystem(PlayerControlSystem) != null) {
        world.deleteSystem(world.getSystem(PlayerControlSystem));
        world.deleteSystem(world.getSystem(WaveSystem));
        world.deleteSystem(world.getSystem(DamageSystem));
      }
    }
    world.process();
  }

  @override
  resize(num width, num height) {
    renderer.resize(width, height);
  }

  @override
  pause() {}

  @override
  resume() {}

  @override
  preload() {}

  void stopMadness() {
    madness = false;
  }

}

class GameMenu extends Menu {

  GameMenu(Game game) : super(game);

  @override
  void bind() {
    subscribe(querySelector('#replay1').onClick.listen(replay));
    subscribe(querySelector('#replay2').onClick.listen(replay));
    subscribe(querySelector('#menu1').onClick.listen(menu));
    subscribe(querySelector('#menu2').onClick.listen(menu));
  }

  @override
  void create() {
    buildFromHtml(game.assetManager.get('ingame.html'));
  }

  void showDeath() {
    querySelector('#death').style.display = "block";
    querySelector('#loss').style.display = "block";
    querySelector('#loss').style.opacity = "1";
    new Tween()
      ..delay = 2.5
      ..callback = () {
        querySelector('#replay1').style.opacity = "1";
        querySelector('#menu1').style.opacity = "1";
      }
      ..start(game.tweenManager);
  }

  void showWin() {
    querySelector('#win').style.display = "block";
    querySelector('#wincover').style.display = "block";
    querySelector('#wincover').style.opacity = "1";
    new Tween()
      ..delay = 2.5
      ..callback = () {
        querySelector('#replay2').style.opacity = "1";
        querySelector('#menu2').style.opacity = "1";
      }
      ..start(game.tweenManager);
  }

  void setHealth(double health) {
    if(health < 0) {
      health = 0.0;
    }
    querySelector('#health').text = "HP: ${health.round()}";
  }

  void setRound(int round) {
    querySelector('#round').text = "${round} / 10";
    querySelector('#countdown').style.display = "none";
  }

  void countdown(int to) {
    var countdown = querySelector('#countdown');
    countdown.style.display = "block";
    countdown.text = "3";
    new Tween()
      ..delay = 1.0
      ..callback = (() => countdown.text = "2")
      ..start(game.tweenManager);
    new Tween()
      ..delay = 2.0
      ..callback = (() => countdown.text = "1")
      ..start(game.tweenManager);
    new Tween()
      ..delay = 3.0
      ..callback = (() => setRound(to))
      ..start(game.tweenManager);
    game.assetManager.get("countdown.ogg").play();
  }

  void replay(MouseEvent e) {
    querySelector('#fade').style.opacity = "1";
    querySelector('#wincover').style.opacity = "0";
    querySelector('#loss').style.opacity = "0";
    new Tween()
      ..delay = 1.0
      ..callback = () {
        game.pushState(new GameState(game));
        querySelector('#fade').style.opacity = "0";
        querySelector('#wincover').style.display = "none";
        querySelector('#loss').style.display = "none";
      }
      ..start(game.tweenManager);
    new Tween()
      ..delay = 5.0
      ..callback = () {
        querySelector('#wincover').style.display = "block";
        querySelector('#loss').style.display = "block";
      }
      ..start(game.tweenManager);
    unbind();
    game.audio.stopAll();
    game.assetManager.get("start.ogg").play();
  }

  void menu(MouseEvent e) {
    querySelector('#fade').style.opacity = "1";
    querySelector('#wincover').style.opacity = "0";
    querySelector('#loss').style.opacity = "0";
    new Tween()
      ..delay = 1.0
      ..callback = () {
        game.pushState(new MainMenuState(game));
        querySelector('#fade').style.opacity = "0";
        querySelector('#wincover').style.display = "none";
        querySelector('#loss').style.display = "none";
      }
      ..start(game.tweenManager);
    new Tween()
      ..delay = 5.0
      ..callback = () {
        querySelector('#wincover').style.display = "block";
        querySelector('#loss').style.display = "block";
      }
      ..start(game.tweenManager);
    unbind();
    game.audio.stopAll();
  }

}