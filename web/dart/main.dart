library game;

import 'dart:html';
import 'package:cobblestone/cobblestone.dart';
import 'package:dartemis/dartemis.dart';
import 'package:yaml/yaml.dart' as YAML;
import 'dart:web_gl' as WebGL;
import 'dart:convert';

part 'game.dart';

part 'game_state.dart';

part 'entity_builder.dart';

part 'components/vector.dart';
part 'components/static_texture.dart';
part 'components/hitbox.dart';
part 'components/transform.dart';
part 'components/flags.dart';
part 'components/health.dart';
part 'components/chain.dart';

part 'systems/render_system.dart';
part 'systems/hitbox_transform_system.dart';
part 'systems/movement_system.dart';
part 'systems/player_control_system.dart';
part 'systems/collision_list_system.dart';
part 'systems/collision_remove_system.dart';
part 'systems/collapse_system.dart';
part 'systems/damage_system.dart';
part 'systems/ai/chandelier_ai_system.dart';
part 'systems/ai/duster_ai_system.dart';
part 'systems/ai/ghost_ai_system.dart';
part 'systems/ai/teapot_ai_system.dart';
part 'systems/ai/knife_ai_system.dart';
part 'systems/ai/skele_ai_system.dart';
part 'systems/solid_list.dart';
part 'systems/wave_system.dart';
part 'systems/acceleration_system.dart';
part 'systems/damage_flash_system.dart';
part 'systems/flip_repeat_system.dart';

part 'menu/menu.dart';
part 'menu/main_menu.dart';

part 'shake_camera.dart';

void main() {
  new Game();
}

loadYaml(String url) async {
  return YAML.loadYaml(await HttpRequest.getString(url));
}
