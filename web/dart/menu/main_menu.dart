part of game;

class MainMenu extends Menu {

  MainMenu(var game): super(game);

  @override
  void bind() {
    subscribe(querySelector('#start').onClick.listen(startGame));
  }

  @override
  void create() {
    buildFromHtml(game.assetManager.get('main.html'));
  }

  @override
  preload() {
    game.assetManager.load('main.html', HttpRequest.getString('html/main.html'));
  }

  void startGame(e) {
    querySelector('#fade').style.opacity = "1";
    new Tween()
      ..delay = 1.0
      ..callback = () {
        game.pushState(new GameState(game));
        querySelector('#fade').style.opacity = "0";
      }
      ..start(game.tweenManager);
    game.audio.stopAll();
    unbind();
    game.assetManager.get("start.ogg").play();
  }

  @override
  show() {
    super.show();
  }

  @override
  hide() {
    super.hide();
  }

}

class MainMenuState extends MenuState {

  Game game;

  SpriteBatch batch;
  Camera2D fullscreen;

  Music title;

  MainMenuState(this.game) : super(new MainMenu(game)) {
    batch = new SpriteBatch.defaultShader(gl);
    fullscreen = new Camera2D.originBottomLeft(width, height);
  }

  @override
  create() {
    super.create();
    title = assetManager.get("title.ogg");
    title.loop();
  }

  @override
  render(num delta) {
    game.gl.clearScreen(Colors.black);
    fullscreen.update();
    batch.projection = fullscreen.combined;
    batch.begin();
    batch.draw(assetManager.get('atlas')['title'], 0, 0, width: width, height: height);
    batch.end();
  }

  @override
  resize(num width, num height) {
    fullscreen = new Camera2D.originBottomLeft(width, height);
  }

  @override
  update(num delta) {

  }

}
