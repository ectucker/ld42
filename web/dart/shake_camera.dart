part of game;

class ShakeCamera extends Camera2D {

  double shakeTime = 0.0;
  double shakeIntensity = 0.7;
  double shakeDecay = 1.0;

  ShakeCamera.originBottomLeft(num width, num height) : super.originBottomLeft(width, height);

  void shake(double time, [double intensity = 0.7, double decay = 1.0]) {
    shakeTime = time;
    shakeIntensity = intensity;
    shakeDecay = decay;
  }

  void updateShake(double delta) {
    if (shakeTime > 0 && delta > 0) {
      setTranslation(new Vector2.random() * shakeIntensity);
      shakeTime -= delta * shakeDecay;
    } else {
      shakeTime = 0.0;
      setTranslation(0.0, 0.0);
    }
  }

}