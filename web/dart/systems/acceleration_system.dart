part of game;

class AccelerationSystem extends EntityProcessingSystem {

  Mapper<Acceleration> accelerationMapper;
  Mapper<Velocity> velocityMapper;
  Mapper<MaxSpeed> speedMapper;

  AccelerationSystem() : super(Aspect.getAspectForAllOf([Acceleration, Velocity]));

  @override
  void initialize() {
    accelerationMapper = new Mapper<Acceleration>(Acceleration, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    speedMapper = new Mapper<MaxSpeed>(MaxSpeed, world);
  }

  @override
  void processEntity(Entity entity) {
    Acceleration acc = accelerationMapper[entity];
    Velocity vel = velocityMapper[entity];

    vel.xy = vel.vec + acc.vec * world.delta;

    if(vel.vec.length < 1) {
      vel.xy = new Vector2.zero();
    }

    if(speedMapper.has(entity)) {
      double maxSpeed = speedMapper[entity].value;

      if(vel.vec.length > maxSpeed) {
        vel.xy = vel.vec.normalized().scaled(maxSpeed);
      }
    }
  }

}