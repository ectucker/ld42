part of game;

class ChandelierAI extends Component {}

class ChandelierAISystem extends EntityProcessingSystem {

  static const double SPEED = 250.0;

  double fireTime = 0.00;

  double fireAngle = 0.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  ChandelierAISystem(this.builder, this.player, this.enemies, this.atlas): super(Aspect.getAspectForAllOf([ChandelierAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];
    StaticTexture tex = textureMapper[entity];

    if(fireTime <= 0.3) {
      tex.setTexture(atlas['candle2'], 'candle2');
    } else {
      tex.setTexture(atlas['candle'], 'candle');
    }

    fireTime -= world.delta;
    if(fireTime <= 0) {
      Vector2 target = positionMapper[player].vec;
      builder.buildChandelierBullets(pos.vec + new Vector2(130.0, 170.0), fireAngle);
      fireTime = enemies['chandelier']['fireRate'];
      fireAngle += 7.0;
    }
  }

}