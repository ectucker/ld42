part of game;

class DusterAI extends Component {}

class DusterAISystem extends EntityProcessingSystem {

  static const double SPEED = 250.0;
  static const int N_SHOTS = 5;

  Vector2 waveTarget;

  double fireTime = 0.0;
  double waveTime = 0.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Flip> flipMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  DusterAISystem(this.builder, this.player, this.enemies, this.atlas): super(Aspect.getAspectForAllOf([DusterAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];

    Hitbox box = entity.getComponentByClass(Hitbox);
    Vector2 center = box.box.center.xy;
    Vector2 playerCenter = positionMapper[player].vec + new Vector2(32.0, 64.0);

    flipMapper[entity].x = (playerCenter - center).x > 0;

    fireTime -= world.delta;
    if(fireTime <= 0.4) {
      textureMapper[entity].setTexture(atlas['duster3'], 'duster3');
    } else if(fireTime <= 0.8) {
      textureMapper[entity].setTexture(atlas['duster2'], 'duster2');
    } else {
      textureMapper[entity].setTexture(atlas['duster1'], 'duster1');
    }
    if(fireTime <= 0) {
      builder.buildDusterBullets(center + (flipMapper[entity].x ? new Vector2(0.0, 0.0) : new Vector2(-110.0, 0.0)), playerCenter);
      fireTime = enemies['duster']['fireRate'];
    }
  }

}