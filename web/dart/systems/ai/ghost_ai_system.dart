part of game;

class GhostAI extends Component {

  double time = 0.0;

}

class GhostAISystem extends EntityProcessingSystem {

  static const double SPEED = 250.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Acceleration> accelerationMapper;
  Mapper<GhostAI> aiMapper;
  Mapper<Flip> flipMapper;
  Mapper<StaticTexture> textureMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  GhostAISystem(this.builder, this.player, this.enemies, this.atlas): super(Aspect.getAspectForAllOf([GhostAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    accelerationMapper = new Mapper<Acceleration>(Acceleration, world);
    aiMapper = new Mapper<GhostAI>(GhostAI, world);
    flipMapper = new Mapper<Flip>(Flip, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];
    Acceleration acc = accelerationMapper[entity];
    GhostAI ai = aiMapper[entity];

    Vector2 center = pos.vec + new Vector2(64.0, 81.0);
    Vector2 playerCenter = positionMapper[player].vec + new Vector2(32.0, 64.0);

    acc.xy = (playerCenter - center).normalized() * SPEED;

    flipMapper[entity].x = acc.x > 0.0;

    ai.time += world.delta;
    if(ai.time > 0.4) {
      textureMapper[entity].setTexture(atlas['ghost2'], 'ghost2');
    }
    if(ai.time > 0.8) {
      textureMapper[entity].setTexture(atlas['ghost3'], 'ghost3');
    }
    if(ai.time > 1.2) {
      textureMapper[entity].setTexture(atlas['ghost1'], 'ghost1');
      ai.time = 0.0;
    }
  }

}