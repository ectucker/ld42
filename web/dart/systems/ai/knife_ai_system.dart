part of game;

class KnifeAI extends Component {}

class KnifeAISystem extends EntityProcessingSystem {

  static const double SPEED = 1500.0;
  static const double FRICTION = 500.0;

  double fireTime = 0.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Rotation> rotationMapper;
  Mapper<Flip> flipMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  KnifeAISystem(this.builder, this.player, this.enemies, this.atlas): super(Aspect.getAspectForAllOf([KnifeAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];
    StaticTexture tex = textureMapper[entity];

    Vector2 center = pos.vec + ((flipMapper[entity].y) ? new Vector2(69.0, 59.0) : new Vector2(60.0, 59.0));
    Vector2 playerCenter = positionMapper[player].vec + new Vector2(32.0, 64.0);

    if(fireTime < 0.5) {
      rotationMapper[entity].value = degrees(
          atan2(playerCenter.y - center.y, playerCenter.x - center.x) + PI);
      flipMapper[entity].y = rotationMapper[entity].value > 90 &&
          rotationMapper[entity].value < 270;
    }

    fireTime -= world.delta;
    if(fireTime <= 0.2) {
      tex.setTexture(atlas["knife2"], "knife2");
    }
    if(fireTime <= 0) {
      tex.setTexture(atlas["knife1"], "knife1");
      builder.buildKnifeBullet(center, playerCenter, flipMapper[entity].y);
      fireTime = enemies['knives']['fireRate'];
    }
  }

}