part of game;

class SkeleAI extends Component {}

class SkeleAISystem extends EntityProcessingSystem {
  static const double SPEED = 1500.0;
  static const double FRICTION = 500.0;

  double fireTime = 0.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Flip> flipMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  Return bone;

  SkeleAISystem(this.builder, this.player, this.enemies, this.atlas)
      : super(Aspect.getAspectForAllOf([SkeleAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];
    StaticTexture tex = textureMapper[entity];

    Vector2 center = pos.vec + new Vector2(32.0, 64.0);
    Vector2 playerCenter = positionMapper[player].vec + new Vector2(32.0, 64.0);

    flipMapper[entity].x = (playerCenter - center).x > 0;

    fireTime -= world.delta;
    if(fireTime <= 0.3) {
      tex.setTexture(atlas['skeleton3'], 'skeleton3');
    } else if(fireTime <= 0.6) {
      tex.setTexture(atlas['skeleton2'], 'skeleton2');
    } else {
      tex.setTexture(atlas['skeleton1'], 'skeleton1');
    }
    if(!hasBone()) {
      tex.setTexture(atlas[tex.name + '_nb'], tex.name + '_nb');
    }
    if (fireTime <= 0) {
      bone = builder.buildSkeleBullet(center, playerCenter, entity);
      fireTime = enemies['skelly']['fireRate'];
    }
  }

  bool hasBone() {
    if(bone == null) {
      return true;
    }
    return bone.timeToKill <= 0.0;
  }

}

class Return extends Component {
  double timeBack;
  double timeToKill;

  Entity source;

  bool bounced = false;

  Return(double firerate, this.source) {
    timeBack = firerate / 2;
    timeToKill = firerate;
  }
}

class ReturnSystem extends EntityProcessingSystem {
  Mapper<Velocity> velocityMapper;
  Mapper<CollisionList> collisionMapper;
  Mapper<Return> returnMapper;
  Mapper<Rotation> rotationMapper;

  ReturnSystem() : super(Aspect.getAspectForAllOf([Return, Velocity]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    collisionMapper = new Mapper<CollisionList>(CollisionList, world);
    returnMapper = new Mapper<Return>(Return, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Return returnTimer = returnMapper[entity];
    CollisionList collision = collisionMapper[entity];

    returnTimer.timeBack -= world.delta;
    returnTimer.timeToKill -= world.delta;

    for (Entity other in collision.others) {
      if ((other.getComponentByClass(Solid) != null ||
          other.getComponentByClass(PlayerControl) != null) &&
              !returnTimer.bounced) {
        vel.xy = vel.vec * -1.0;
        returnTimer.timeBack = 0.0;
        returnTimer.bounced = true;
        collision.paused = true;
        return;
      }
      if (returnTimer.timeBack <= 0 && other == returnTimer.source) {
        returnTimer.timeToKill = 0.0;
        entity.deleteFromWorld();
        return;
      }
    }

    if (returnTimer.timeBack <= 0.0 && !returnTimer.bounced) {
      vel.xy = vel.vec * -1.0;
      returnTimer.bounced = true;
      collision.paused = true;
      return;
    }

    if (returnTimer.timeToKill <= 0) {
      entity.deleteFromWorld();
    }

    rotationMapper[entity].value += 250 * world.delta;
  }
}
