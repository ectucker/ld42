part of game;

class TeapotAI extends Component {}

class TeapotAISystem extends EntityProcessingSystem {

  static const double SPEED = 1500.0;
  static const double FRICTION = 500.0;
  static const double BURST_TIME = 5.0;

  double fireTime = 0.0;
  double burstTime = 0.0;

  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<Acceleration> accelerationMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Flip> flipMapper;

  EntityBuilder builder;

  Entity player;

  var enemies;

  var atlas;

  TeapotAISystem(this.builder, this.player, this.enemies, this.atlas): super(Aspect.getAspectForAllOf([TeapotAI, Velocity, Position]));

  @override
  void initialize() {
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    accelerationMapper = new Mapper<Acceleration>(Acceleration, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    flipMapper = new Mapper<Flip>(Flip, world);
  }

  @override
  void processEntity(Entity entity) {
    Velocity vel = velocityMapper[entity];
    Position pos = positionMapper[entity];
    Acceleration acc = accelerationMapper[entity];

    flipMapper[entity].x = positionMapper[player].x > pos.x + 84;

    Vector2 center = pos.vec + (flipMapper[entity].x ? new Vector2(30.0 + 84.0, 86.0) : new Vector2(-30.0, 86.0));
    Vector2 playerCenter = positionMapper[player].vec + new Vector2(32.0, 64.0);

    fireTime -= world.delta;
    burstTime -= world.delta;
    if(burstTime > 0) {
      acc.xy = -vel.vec.normalized() * FRICTION;
      if(fireTime < 0.4) {
        textureMapper[entity].setTexture(atlas['teapot2'], 'teapot2');
      }
      if(fireTime <= 0) {
        builder.buildTeapotBullets(center, playerCenter, flipMapper[entity].x);
        fireTime = enemies['teapot']['fireRate'];
        textureMapper[entity].setTexture(atlas['teapot1'], 'teapot1');
      }
    } else if(center.distanceTo(playerCenter) > 400) {
      acc.xy = (playerCenter - center).normalized() * SPEED;
    } else {
      burstTime = BURST_TIME;
    }
  }

}