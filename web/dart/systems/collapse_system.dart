part of game;

class CollapseSystem extends EntityProcessingSystem {

  static const double STEP_SIZE = 0.01;

  EntityBuilder builder;
  Entity player;

  Mapper<CollisionList> collisionMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Solid> solidMapper;
  Mapper<Position> positionMapper;
  Mapper<Velocity> velocityMapper;

  var map = new List.generate(31, (_) => new List.generate(14, (_) => 0));

  CollapseSystem(this.builder, this.player) : super(Aspect.getAspectForAllOf([Velocity, CollisionList, Collapse, Position])) {}

  @override
  void initialize() {
    collisionMapper = new Mapper<CollisionList>(CollisionList, world);
    solidMapper = new Mapper<Solid>(Solid, world);
    positionMapper = new Mapper<Position>(Position, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  void processEntity(Entity entity) {
    CollisionList collision = collisionMapper[entity];

    for(var other in collision.others) {
      if(solidMapper.has(other)) {
        Obb3 box = hitboxMapper[entity].box;
        Vector2 vel = velocityMapper[entity].vec;
        while(intersectsSolid(box, collision.others)) {
          box.center.sub(new Vector3(vel.x, vel.y, 0.0) * STEP_SIZE);
        }
        builder.buildCollapse(map, (box.center - box.halfExtents).xy);
        gAssetManager.get("collapse.ogg").play();
        RenderSystem renderer = world.getSystem(RenderSystem);
        renderer.shake(1.0, 6.0);
      }
    }
  }

  bool intersectsSolid(Obb3 box, List<Entity> potentialCollisions) {
    for(Entity other in potentialCollisions) {
      Obb3 otherBox = hitboxMapper[other].box;
      if(otherBox.intersectsWithObb3(box) && solidMapper.has(other)) {
        return true;
      }
    }
    return false;
  }

}