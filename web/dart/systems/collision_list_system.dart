part of game;

class Collision extends Component {}

class CollisionList extends Component {

  List<Entity> others = [];

  bool paused = false;

  CollisionList();

}

class CollisionListSystem extends EntitySystem {

  Mapper<Hitbox> hitboxMapper;
  Mapper<CollisionList> collisionListMapper;

  CollisionListSystem() : super(Aspect.getAspectForAllOf([Hitbox]).oneOf([CollisionList, Collision])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    collisionListMapper = new Mapper<CollisionList>(CollisionList, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    for(Entity entity in entities) {
      if(collisionListMapper.has(entity)) {
          collisionListMapper[entity].others.clear();

          for (Entity other in entities) {
            Obb3 entityBox = hitboxMapper[entity].box;
            Obb3 otherBox = hitboxMapper[other].box;
            if (otherBox.center.distanceToSquared(entityBox.center) <
                pow(max(max(entityBox.halfExtents.x, otherBox.halfExtents.x),
                    max(entityBox.halfExtents.y, otherBox.halfExtents.y)) * 2,
                    2)) {
              if (entityBox.intersectsWithObb3(otherBox) &&
                  otherBox != entityBox) {
                collisionListMapper[entity].others.add(other);
              }
            }
          }
      }
    }
  }

}
