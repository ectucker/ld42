part of game;

class CollisionRemoveSystem extends EntityProcessingSystem {

  Mapper<CollisionList> collisionMapper;
  Mapper<Solid> solidMapper;
  Mapper<Health> healthMapper;

  CollisionRemoveSystem() : super(Aspect.getAspectForAllOf([CollisionList, CollisionRemove])) {}

  @override
  void initialize() {
    collisionMapper = new Mapper<CollisionList>(CollisionList, world);
    solidMapper = new Mapper<Solid>(Solid, world);
    healthMapper = new Mapper<Health>(Health, world);
  }

  @override
  void processEntity(Entity entity) {
    CollisionList collision = collisionMapper[entity];

    for(var other in collision.others) {
      if(solidMapper.has(other)) {
        entity.deleteFromWorld();
      }
    }
  }

}
