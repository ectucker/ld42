part of game;

class DamageFlashSystem extends EntityProcessingSystem {

  Mapper<StaticTexture> textureMapper;
  Mapper<Health> healthMapper;

  var atlas;

  DamageFlashSystem(this.atlas) : super(Aspect.getAspectForAllOf([StaticTexture, Health]));

  @override
  void initialize() {
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    healthMapper = new Mapper<Health>(Health, world);
  }

  @override
  void processEntity(Entity entity) {
    StaticTexture tex = textureMapper[entity];
    Health hp = healthMapper[entity];

    hp.flashTime -= world.delta;
    if(tex.name.contains("_flash")) {
      if (hp.flashTime < 0) {
        String name = tex.name.replaceAll("_flash", "");
        tex.setTexture(atlas[name], name);
      }
    } else {
      if(hp.flashTime > 0) {
        String name = tex.name + "_flash";
        tex.setTexture(atlas[name], name);
      }
    }
  }

}