part of game;

class DamageSystem extends EntityProcessingSystem {

  Mapper<CollisionList> collisionMapper;
  Mapper<Damage> damageMapper;
  Mapper<Health> healthMapper;
  Mapper<PlayerControl> playerMapper;

  int hits = 0;

  GameMenu menu;

  GameState state;

  DamageSystem(this.menu, this.state) : super(Aspect.getAspectForAllOf([CollisionList, Health])) {}

  @override
  void initialize() {
    collisionMapper = new Mapper<CollisionList>(CollisionList, world);
    damageMapper = new Mapper<Damage>(Damage, world);
    healthMapper = new Mapper<Health>(Health, world);
    playerMapper = new Mapper<PlayerControl>(PlayerControl, world);
  }

  @override
  void processEntity(Entity entity) {
    CollisionList collision = collisionMapper[entity];
    Health health = healthMapper[entity];

    for(var other in collision.others) {
      if(damageMapper.has(other)) {
        Damage damage = damageMapper[other];
        if(damage.fromPlayer != health.player) {
          if(!health.player || !playerMapper[entity].rolling) {
            health.value -= damageMapper[other].value;
            health.flashTime = 0.2;
            if(health.player) {
              hits++;
              menu.setHealth(health.value);
              if(damage.value > 0) {
                gAssetManager.get("hurt.ogg").play();
                RenderSystem renderer = world.getSystem(RenderSystem);
                renderer.shake(0.5, 12.0);
              }
            }
            if (health.value <= 0 && !health.player) {
              health.death?.play();
              entity.deleteFromWorld();
            } else if (health.value <= 0) {
              menu.showDeath();
              state.stopMadness();
            } else {
              health.hit?.play();
            }
            if(!damage.bouncing) {
              other.deleteFromWorld();
            } else {
              damage.value = 0.0;
            }
          }
        }
      }
    }
  }

}
