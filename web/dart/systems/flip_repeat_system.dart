part of game;

class FlipRepeat extends Component {
  
  double time = 0.0;
  
  FlipRepeat();
  
}

class FlipRepeatSystem extends EntityProcessingSystem {
  
  Mapper<Flip> flipMapper;
  Mapper<FlipRepeat> repeatMapper;
  
  FlipRepeatSystem() : super(Aspect.getAspectForAllOf([FlipRepeat, Flip]));
  
  @override
  void initialize() {
    flipMapper = new Mapper<Flip>(Flip, world);
    repeatMapper = new Mapper<FlipRepeat>(FlipRepeat, world);
  }
  
  @override
  void processEntity(Entity entity) {
    FlipRepeat repeat = repeatMapper[entity];
    
    repeat.time += world.delta;
    if(repeat.time >= 0.3) {
      repeat.time = 0.0;
      flipMapper[entity].x = !flipMapper[entity].x;
    }
  }
}