part of game;

class HitboxTransformSystem extends EntityProcessingSystem {

  Mapper<Hitbox> hitboxMapper;
  Mapper<Position> positionMapper;
  Mapper<Rotation> rotationMapper;

  HitboxTransformSystem() : super(Aspect.getAspectForAllOf([Hitbox, Position]));

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
    positionMapper = new Mapper<Position>(Position, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
  }

  @override
  void processEntity(Entity entity) {
    Hitbox box = hitboxMapper[entity];
    Position pos = positionMapper[entity];

    box.xy = pos.vec;

    if(rotationMapper.has(entity)) {
      box.angle = rotationMapper[entity].value;
    }
  }

}