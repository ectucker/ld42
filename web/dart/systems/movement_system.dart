part of game;

class MovementSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Velocity> velocityMapper;

  MovementSystem() : super(Aspect.getAspectForAllOf([Position, Velocity]));

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
  }

  @override
  void processEntity(Entity entity) {
    Position pos = positionMapper[entity];
    Velocity vel = velocityMapper[entity];

    pos.xy = pos.vec + vel.vec * world.delta;
  }

}

class AntiMovementSystem extends EntityProcessingSystem {

  Mapper<Position> positionMapper;
  Mapper<Velocity> velocityMapper;
  Mapper<CollisionList> collisionMapper;
  Mapper<Solid> solidMapper;

  AntiMovementSystem() : super(Aspect.getAspectForAllOf([Position, Velocity, CollisionList]));

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    collisionMapper = new Mapper<CollisionList>(CollisionList, world);
    solidMapper = new Mapper<Solid>(Solid, world);
  }

  @override
  void processEntity(Entity entity) {
    Position pos = positionMapper[entity];
    Velocity vel = velocityMapper[entity];
    CollisionList collision = collisionMapper[entity];

    if(!collision.paused) {
      for (Entity entity in collision.others) {
        if (solidMapper.has(entity)) {
          pos.xy = pos.vec + -vel.vec * world.delta;
          return;
        }
      }
    }
  }

}