part of game;

class PlayerControl extends Component {

  bool facingLeft = false;
  bool rolling = false;

  PlayerControl();

}

class PlayerControlSystem extends EntityProcessingSystem {

  static const double SPEED = 1500.0;
  static const double FRICTION = 500.0;
  static const double FIRE_TIME = 0.3;
  static const double ROLL_TIME = 0.25;

  double fireTime = FIRE_TIME;

  bool rolling = false;
  Vector2 rollDir;
  double rollTime = 0.0;
  double frameTime = 0.0;

  Mapper<Acceleration> accelerationMapper;
  Mapper<Velocity> velocityMapper;
  Mapper<Position> positionMapper;
  Mapper<PlayerControl> controlMapper;
  Mapper<Rotation> rotationMapper;
  Mapper<Flip> flipMapper;
  Mapper<StaticTexture> textureMapper;

  Keyboard keyboard;
  Mouse mouse;

  EntityBuilder builder;

  RenderSystem renderSystem;

  TweenManager tweenManager;

  bool stop = false;

  var atlas;

  PlayerControlSystem(this.keyboard, this.mouse, this.builder, this.renderSystem, this.tweenManager, this.atlas): super(Aspect.getAspectForAllOf([PlayerControl, Velocity, Position]));

  @override
  void initialize() {
    accelerationMapper = new Mapper<Acceleration>(Acceleration, world);
    velocityMapper = new Mapper<Velocity>(Velocity, world);
    positionMapper = new Mapper<Position>(Position, world);
    controlMapper = new Mapper<PlayerControl>(PlayerControl, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
    flipMapper = new Mapper<Flip>(Flip, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
  }

  @override
  void processEntity(Entity entity) {
    if(!stop) {
      Acceleration acc = accelerationMapper[entity];
      Velocity vel = velocityMapper[entity];
      Position pos = positionMapper[entity];
      PlayerControl control = controlMapper[entity];
      StaticTexture tex = textureMapper[entity];

      /*Weapon weapon = entity.getComponentByClass(Weapon);
      Entity gun = weapon.attached;

      Position gunPos = positionMapper[gun];
      Rotation gunRot = rotationMapper[gun];
      Flip gunFlip = flipMapper[gun];
      StaticTexture gunTex = textureMapper[gun];

      gunPos.xy = pos.vec + new Vector2(-32.0, 2.0);
      Vector2 gunCenter = gunPos.vec + new Vector2(68.0, 68.0);*/
      Vector2 gunCenter = pos.vec + new Vector2(-11.0, 0.0);
      Vector2 target = renderSystem.mouseToScreen(
          new Vector2(mouse.x.toDouble(), mouse.y.toDouble())) - new Vector2(24.0 + 20.0, 54.0 + 22.0);

      if (!rolling) {
        if (keyboard.keyPressed(KeyCode.W)) {
          acc.y = SPEED;
        } else if (keyboard.keyPressed(KeyCode.S)) {
          acc.y = -SPEED;
        } else {
          acc.y = -vel.y.sign * FRICTION;
        }
        if (keyboard.keyPressed(KeyCode.A)) {
          acc.x = -SPEED;
          control.facingLeft = true;
        } else if (keyboard.keyPressed(KeyCode.D)) {
          acc.x = SPEED;
          control.facingLeft = false;
        } else {
          acc.x = -vel.x.sign * FRICTION;
        }

        /*
        if (fireTime <= 0) {
          gunRot.value =
              degrees(atan2(target.y - gunCenter.y, target.x - gunCenter.x));
          gunFlip.y = gunRot.value.abs() > 90;
          if(gunTex.name.contains('_b')) {
            if(gunFlip.y) {
              gunRot.value -= 45;
            } else {
              gunRot.value += 45;
            }
          }
          if(gunTex.name.contains('_f')) {
            if(gunFlip.y) {
              gunRot.value -= 45;
            } else {
              gunRot.value += 45;
            }
          }
        }*/

        fireTime -= world.delta;
        if (mouse.leftDown && fireTime <= 0) {
          builder.buildPlayerBullet(
              gunCenter + (target - gunCenter).normalized().scaled(42.0),
              target);
          fireTime = FIRE_TIME;
          /*var back = new Tween()
            ..get = [() => gunRot.value]
            ..set = [(value) => gunRot.value = value]
            ..target = [gunRot.value]
            ..duration = 0.2
            ..ease = expoIn;
          new Tween()
            ..get = [() => gunRot.value]
            ..set = [(value) => gunRot.value = value]
            ..target = [gunRot.value + 15 * (gunRot.value.abs() > 90 ? -1 : 1)]
            ..duration = 0.2
            ..ease = expoOut
            ..chain(back)
            ..start(tweenManager);*/
          gAssetManager.get("gunshot.ogg").play();
        }

        if (keyboard.keyJustTyped(KeyCode.SPACE)) {
          rolling = true;
          tex.setTexture(null, 'player_roll1');
          rollDir = target - gunCenter - new Vector2(0.0, 32.0);
          MaxSpeed max = entity.getComponentByClass(MaxSpeed);
          max.value = 1000.0;
          acc.xy = vel.vec.normalized() * SPEED * 2.0;
          gAssetManager.get("roll.ogg").play();
        }
      } else {
        rollTime += world.delta;
        if (rollTime >= ROLL_TIME) {
          rollTime = 0.0;
          rolling = false;
          MaxSpeed max = entity.getComponentByClass(MaxSpeed);
          max.value = 200.0;
        }
      }

      frameTime += world.delta;
      if(rolling && rollTime > 0.1) {
        tex.setTexture(null, 'player_roll2');
      } else if(rolling) {
        tex.setTexture(null, 'player_roll1');
      }
      if(!rolling) {
        if(frameTime < 0.2) {
          tex.setTexture(null, 'player1');
        } else if(frameTime < 0.4) {
          tex.setTexture(null, 'player2');
        } else {
          tex.setTexture(null, 'player2');
          frameTime = 0.0;
        }
      }

      String direction = '_b';
      if(acc.y > 1000) {
        direction = '_f';
      }
      if(acc.x.abs() > 1000) {
        direction = '_s';
      }

      Flip flip = entity.getComponentByClass(Flip);
      flip.x = acc.x < -1000;

      tex.setTexture(atlas[tex.name + direction], tex.name + direction);
      /*gunTex.setTexture(atlas['staff' + direction], 'staff' + direction);
      if(rolling) {
        gunTex.setTexture(atlas['nope'], 'nope');
      }*/

      control.rolling = rolling;
    }
  }

}