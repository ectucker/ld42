part of game;

class RenderSystem extends EntitySystem {

  GLWrapper gl;

  AssetManager assetManager;

  int width;
  int height;
  CanvasElement canvas;

  Framebuffer screenBuffer;

  List drawLater = [];

  SpriteBatch batch;
  PhysboxBatch debugBatch;

  Camera2D fullscreen;
  ShakeCamera camera;

  List<Vector3> lightPos = [];
  List<Vector4> lightColor = [];

  Vector4 ambientColor = new Vector4(0.6, 0.6, 1.0, 0.5);
  Vector3 attenuation = new Vector3(0.4, 3.0, 20.0);

  Mapper<Position> positionMapper;
  Mapper<StaticTexture> textureMapper;
  Mapper<Hitbox> hitboxMapper;
  Mapper<Flip> flipMapper;
  Mapper<Scale> scaleMapper;
  Mapper<Rotation> rotationMapper;

  RenderSystem(this.gl, this.width, this.height, this.canvas, this.assetManager) : super(Aspect.getAspectForAllOf([Position]).oneOf([StaticTexture, Hitbox])) {
    lightPos.add(new Vector3(0.5, 0.5, 0.2));
    lightColor.add(Colors.white);

    //batch = new SpriteBatch(gl, assetManager.get("lighting"));
    batch = new SpriteBatch.defaultShader(gl);
    batch.setAdditionalUniforms = () {
      batch.setUniform('uLightPos', lightPos);
      batch.setUniform('uLightColor', lightColor);
      batch.setUniform('uAmbientLightColor', ambientColor);
      batch.setUniform('uScreenRes', new Vector2(width.toDouble(), height.toDouble()));
      batch.setUniform('uFalloff', attenuation);
      //batch.setUniform('uNormal', wallNorm.bind(2));
    };
    debugBatch = new PhysboxBatch.defaultShader(gl);
    resize(width, height);
  }

  @override
  void initialize() {
    positionMapper = new Mapper<Position>(Position, world);
    textureMapper = new Mapper<StaticTexture>(StaticTexture, world);
    flipMapper = new Mapper<Flip>(Flip, world);
    scaleMapper = new Mapper<Scale>(Scale, world);
    rotationMapper = new Mapper<Rotation>(Rotation, world);
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }


  @override
  void processEntities(Iterable<Entity> entities) {
    begin();

    batch.draw(assetManager.get('atlas')['background'], 0.0, 0.0);

    List<Entity> drawOrder = entities.toList();
    drawOrder.sort((Entity a, Entity b) => positionMapper[a].y.compareTo(positionMapper[b].y));
    drawOrder = drawOrder.reversed;

    processEntity(drawOrder.last);
    for(Entity draw in drawOrder) {
      processEntity(draw);
    }

    end();
  }

  void begin() {
    camera.update();
    camera.updateShake(world.delta);
    fullscreen.update();

    gl.setGLViewport(width, height);

    screenBuffer.beginCapture();

    gl.clearScreen(Colors.black);
    gl.context.disable(WebGL.DEPTH_TEST);
    gl.context.enable(WebGL.BLEND);
    gl.context.blendFunc(WebGL.SRC_ALPHA, WebGL.ONE_MINUS_SRC_ALPHA);

    batch.projection = camera.combined;
    batch.begin();
  }

  void processEntity(Entity entity) {
    Position position = positionMapper[entity];

    bool flipX, flipY = false;
    if(flipMapper.has(entity)) {
      flipX = flipMapper[entity].x;
      flipY = flipMapper[entity].y;
    }

    double scale = 1.0;
    if(scaleMapper.has(entity)) {
      scale = scaleMapper[entity].value;
    }

    double rot = 0.0;
    if(rotationMapper.has(entity)) {
      rot = rotationMapper[entity].value;
    }

    if(textureMapper.has(entity)) {
      batch.draw(textureMapper[entity].texture, position.x, position.y,
          flipX: flipX, flipY: flipY, scaleX: scale, scaleY: scale, angle: radians(rot));
    }

    if(hitboxMapper.has(entity)) {
      //drawLater.add(hitboxMapper[entity].box);
    }
  }

  @override
  void end() {
    batch.end();

    debugBatch.projection = camera.projection;
    debugBatch.begin();
    for(var box in drawLater) {
      debugBatch.draw2D(box);
    }
    debugBatch.end();
    drawLater.clear();

    screenBuffer.endCapture();

    gl.setGLViewport(canvas.width, canvas.height);

    screenBuffer.render(fullscreen.combined, 0, 0, width, height);
  }

  resize(int width, int height) {
    this.width = width;
    this.height = height;
    this.canvas = canvas;

    screenBuffer = new Framebuffer(gl, width, height, filter: WebGL.LINEAR);
    fullscreen = new Camera2D.originBottomLeft(width, height);
    camera = new ShakeCamera.originBottomLeft(width, height);
  }

  Vector2 mouseToScreen(Vector2 mouse) {
    var rect = canvas.getBoundingClientRect();
    return new Vector2((mouse.x - rect.left) * (width / canvas.width), (mouse.y - rect.top) * (height / canvas.height));
  }

  void shake(double time, [double intensity = 0.7, double decay = 1.0]) {
    camera.shake(time, intensity, decay);
  }

  @override
  bool checkProcessing() => true;

}
