part of game;

class SolidList extends EntitySystem {

  EntityBuilder builder;
  
  Mapper<Hitbox> hitboxMapper;
  
  SolidList(this.builder) : super(Aspect.getAspectForAllOf([Hitbox, Solid])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    builder.solids = entities.map<Hitbox>((Entity e) => hitboxMapper[e]).toList();
  }

}

class HostileList extends EntitySystem {

  EntityBuilder builder;

  Mapper<Hitbox> hitboxMapper;

  HostileList(this.builder) : super(Aspect.getAspectForAllOf([Hostile])) {}

  @override
  void initialize() {
    hitboxMapper = new Mapper<Hitbox>(Hitbox, world);
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    builder.hostiles = entities.map<Hitbox>((Entity e) => hitboxMapper[e]).toList();
    builder.hostileEntities = entities.toList();
  }

}

class DamageList extends EntitySystem {

  WaveSystem waves;

  DamageList(this.waves) : super(Aspect.getAspectForAllOf([Damage])) {}

  @override
  void initialize() {}

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    waves.bullets = entities;
  }

}