part of game;

class WaveSystem extends EntitySystem {

  var waves;

  EntityBuilder builder;
  Map<String, Function> enemyBuilds;

  int currentWave = 1;
  double currentWaveTime = 0.0;
  double maxWaveTime;

  GameMenu menu;

  bool stop = false;

  Entity player;

  Iterable<Entity> bullets;

  GameState state;

  WaveSystem(var waveData, this.builder, this.menu, this.player, this.state) : super(Aspect.getAspectForAllOf([Hostile])) {
    waves = JSON.decode(JSON.encode(waveData));
    enemyBuilds = {
      'chandelier': () => builder.buildChandelier(),
      'ghost': () => builder.buildGhost(),
      'teapot': () => builder.buildTeapot(),
      'duster': () => builder.buildDuster(),
      'knives': () => builder.buildKnife(),
      'skelly': () => builder.buildSkele()
    };

    maxWaveTime = calcMaxWaveTime();
  }

  @override
  bool checkProcessing() => true;

  @override
  void processEntities(Iterable<Entity> entities) {
    if(!stop) {
      currentWaveTime += world.delta;
      for (var spawn in waves["wave${currentWave}"]) {
        if (spawn['time'] <= currentWaveTime && !spawn['spawned']) {
          spawn['spawned'] = true;
          enemyBuilds[spawn['type']]();
        }
      }

      if (currentWaveTime > maxWaveTime && entities.length == 0) {
        DamageSystem dmg = world.getSystem(DamageSystem);
        print("Took ${dmg.hits} on round ${currentWave}");
        if (currentWave >= 10) {
          menu.showWin();
          stop = true;
          state.stopMadness();
        } else {
          currentWaveTime = -5.0;
          maxWaveTime = calcMaxWaveTime();
          dmg.hits = 0;
          currentWave++;
          Health hp = player.getComponentByClass(Health);
          hp.value = 100.0;
          menu.setHealth(hp.value);
          menu.countdown(currentWave);
        }

        for(Entity bullet in bullets) {
          if(bullet.getComponentByClass(Collapse) == null) {
            bullet.deleteFromWorld();
          }
        }
      }
    }
  }

  double calcMaxWaveTime() {
    double maxTime = 0.0;
    for(var spawn in waves["wave${currentWave}"]) {
      if(spawn['time'] >= maxTime) {
        maxTime = spawn['time'].toDouble();
      }
    }
    return maxTime + 3.0;
  }

}