precision highp float;

uniform sampler2D uTexture;
uniform vec4 uColor;

varying vec2 vTextureCoord;

void main(void) {
    gl_FragColor = texture2D(uTexture, texCoord);
}